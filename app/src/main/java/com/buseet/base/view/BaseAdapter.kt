package com.buseet.base.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.buseet.BR
import timber.log.Timber

abstract class BaseAdapter<T : BaseParcelable>(
    private val itemClick: (T) -> Unit = {}
) : ListAdapter<T, BaseViewHolder<T>>(BaseItemCallback()) {

    private val mListOfItems = arrayListOf<T?>()

    private var position: Int = 0

    @LayoutRes
    abstract fun layoutRes(): Int
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            layoutRes(),
            parent,
            false
        )
        return BaseViewHolder<T>(binding).apply {
            itemView.setOnClickListener {
                this@BaseAdapter.position = adapterPosition
                itemClick(getItem(adapterPosition))
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    fun removeItem(item: T?, isListEmpty: (Boolean) -> Unit = {}) {
        Timber.e("deleting > $item in pos: $position")
        item?.let {
            mListOfItems.remove(it)
            submitList(mListOfItems.toMutableList())
            isListEmpty(mListOfItems.size == 0)
        }
    }

    fun updateList(newList: List<T>) {
        mListOfItems.addAll(newList)
        submitList(mListOfItems.toMutableList())
    }

    fun setList(newList: List<T?>?) {
        mListOfItems.clear()
        mListOfItems.addAll(newList ?: emptyList())
        submitList(mListOfItems.toMutableList())
    }

    fun addItemToList(item: T?, isAdded: (Boolean) -> Unit) {
        item?.let {
            mListOfItems.add(item)
            submitList(mListOfItems.toMutableList())
            isAdded(true)
        } ?: Timber.e("item is null")
    }

    fun editItem(item: T?) {
        item?.let {
            mListOfItems[position] = item
            submitList(mListOfItems.toMutableList())
        } ?: Timber.e("item is null")
    }

    fun clearCurrentList() {
        mListOfItems.clear()
        submitList(mListOfItems.toMutableList())
    }

}

class BaseViewHolder<T>(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: T) {
        binding.setVariable(BR.item, item)
        binding.executePendingBindings()
    }
}

class BaseItemCallback<T : BaseParcelable> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) =
        oldItem.unique().toString() == newItem.unique().toString()

    override fun areContentsTheSame(oldItem: T, newItem: T) =
        oldItem.equals(newItem)
}

interface BaseParcelable {
    fun unique(): Any
}
