package com.buseet.base

import com.buseet.base.response.NetworkResponse
import com.buseet.model.ErrorResponse
import com.buseet.ui.fragment.HomeResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search")
    suspend fun getVenues(
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("v") version: String,
        @Query("ll") latLong: String,
        @Query("categoryId") categoryId: String?,
    ): NetworkResponse<HomeResponse, ErrorResponse>

}