package com.buseet.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ErrorResponse(

	@field:SerializedName("meta")
	val meta: Meta? = null
) : Parcelable

@Parcelize
data class Meta(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("errorType")
	val errorType: String? = null,

	@field:SerializedName("requestId")
	val requestId: String? = null,

	@field:SerializedName("errorDetail")
	val errorDetail: String? = null
) : Parcelable
