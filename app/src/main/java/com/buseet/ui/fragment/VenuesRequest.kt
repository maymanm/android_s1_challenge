package com.buseet.ui.fragment

import com.buseet.util.Constants
import com.google.android.gms.maps.model.LatLng

data class VenuesRequest(
    val clientId: String = Constants.CLIENT_ID,
    val clientSecret: String = Constants.CLIENT_SECRET,
    var version: String = Constants.VERSION,
    var categoryId: String? = Constants.CATEGORY_ID,
    var latLong: LatLng = LatLng(0.0, 0.0),
)
