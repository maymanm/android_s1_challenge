package com.buseet.ui.fragment

import com.buseet.R
import com.buseet.base.view.BaseAdapter

class VenuesAdapter(itemClick: (HomeItem) -> Unit) : BaseAdapter<HomeItem>(itemClick) {
    override fun layoutRes(): Int = R.layout.item_home_view

}