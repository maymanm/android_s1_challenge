package com.buseet.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.buseet.R
import com.buseet.base.view.BaseFragment
import com.buseet.databinding.FragmentHomeBinding
import com.buseet.util.*
import com.buseet.util.MapUtil.isGPSEnabled
import com.buseet.util.PermissionUtil.requestAppPermissions
import com.buseet.util.Status.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), OnMapReadyCallback,
    ActivityResultCallback<ActivityResult> {

    override val mViewModel: HomeViewModel by viewModels()
    private val fusedLocationClient: FusedLocationProviderClient? by lazy {
        LocationServices.getFusedLocationProviderClient(requireActivity())
    }
    private var locationCallback: LocationCallback? = null
    lateinit var permissionActivityResult: ActivityResultLauncher<Intent>
    private lateinit var gpsActivityResult: ActivityResultLauncher<Intent>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionActivityResult = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(), this
        )
        gpsActivityResult = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            askForLocationPermissions()
        }
    }

    override fun onCreateView(savedInstanceState: Bundle?) {
        initMap()
        mViewModel.apply {
            observe(mutableLiveData) {
                when (it) {
                    Constants.GET_LOCATION_CODE -> startGetLocation()
                }
            }
            observe(resultLiveData) {
                when (it?.status) {
                    SUCCESS -> {
                        showProgress(false)
                    }
                    MESSAGE -> {
                        showProgress(false)
                        requireActivity().showErrorDialog(it.message)
                    }
                    LOADING -> showProgress()
                }
            }
        }
    }

    private fun initMap() {
        val mapFragment: SupportMapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(gm: GoogleMap) {
        mViewModel.onMapReady(gm)
        startGetLocation()
    }

    private fun startGetLocation() {
        if (PermissionUtil.hasAllLocationPermissions(requireContext()) && requireActivity().isGPSEnabled()) {
            locationCallback =
                MapUtil.requestLocationUpdates(requireActivity(), fusedLocationClient) {
                    val latLng = LatLng(it.latitude, it.longitude)
                    mViewModel.getVenues(latLng)
                    mViewModel.setCurrentMarker(latLng)
                    stopLocationUpdates()
                }
        } else {
            MapUtil.openGPSFromSettings(gpsActivityResult)
        }
    }


    private fun askForLocationPermissions() {
        if (PermissionUtil.hasAllLocationPermissions(requireContext())) {
            startGetLocation()
        } else {
            requireActivity().requestAppPermissions(PermissionUtil.getAllLocationPermissions()) {
                when (it) {
                    PermissionUtil.AppPermissionResult.ALL_GOOD -> startGetLocation()
                    PermissionUtil.AppPermissionResult.OPEN_SETTING -> PermissionUtil.goToSettingsPermissions(
                        requireActivity(), permissionActivityResult)
                    PermissionUtil.AppPermissionResult.FAIL -> askForLocationPermissions()
                }
            }
        }
    }

    private fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient?.removeLocationUpdates(it)
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
    }

    override fun onActivityResult(result: ActivityResult?) {
        askForLocationPermissions()
    }

    override fun onStart() {
        getOnBackClick {
            if (!mViewModel.isInFirstState) {
                mViewModel.returnToFirstState()
            } else {
                it.isEnabled = false
                closeFragment()
            }
        }
        super.onStart()
    }
}

