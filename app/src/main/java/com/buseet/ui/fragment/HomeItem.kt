package com.buseet.ui.fragment

import com.buseet.base.view.BaseParcelable

data class HomeItem(
    val name: String? = null,
    val type: String? = null,
    val image: String? = null,
    val lat: Double? = null,
    val lng: Double? = null,
    val distance:Int?=null
) : BaseParcelable {
    override fun unique() = name ?: ""
}
