package com.buseet.ui.fragment

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.buseet.base.AndroidBaseViewModel
import com.buseet.util.Constants
import com.buseet.util.MapUtil.animateCameraToPosition
import com.buseet.util.MapUtil.getMarker
import com.buseet.util.MapUtil.moveMarkerToPosition
import com.buseet.util.Resource
import com.buseet.util.requestCallFlow
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import timber.log.Timber
import javax.inject.Inject

@InternalCoroutinesApi
@HiltViewModel
class HomeViewModel @Inject constructor(
    app: Application,
    private val homeRepository: HomeRepository,
) :
    AndroidBaseViewModel(app), GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener {
    var isInFirstState = true
    private var turnOnCameraIdleFun: Boolean = false
    val currentItem = MutableLiveData<HomeItem?>()
    val refreshButtonVisible = ObservableBoolean(true)
    private val request = VenuesRequest()
    val adapter = VenuesAdapter(::onItemClick)
    private var mMap: GoogleMap? = null
    private var currentMarker: Marker? = null
    private var vMarker: Marker? = null //Selected Venue Marker
    private fun onItemClick(item: HomeItem) {
        if (item.lat != null && item.lng != null) {
            currentItem.value = item
            notifyChange()
            turnOnCameraIdleFun = false
            val latLng = LatLng(item.lat, item.lng)
            setSelectedMarker(latLng)
        }
    }

    fun onRefreshClick() {
        setValue(Constants.GET_LOCATION_CODE)
    }


    fun getVenues(it: LatLng) {
        refreshButtonVisible.set(false)
        request.latLong = it
        requestCallFlow({ homeRepository.getVenues(request) }) {
            postResult(Resource.success())
            mapToSpecificData(it.response?.venues)
        }
    }

    private fun mapToSpecificData(venues: List<VenuesItem?>?) {
        val list = arrayListOf<HomeItem>()
        venues?.sortedBy { it?.location?.distance }?.forEach {
            list.add(HomeItem(name = it?.name,
                type = it?.categories?.get(0)?.name,
                image = "${it?.categories?.get(0)?.icon?.prefix}${it?.categories?.get(0)?.icon?.suffix}",
                lat = it?.location?.lat,
                lng = it?.location?.lng,
                distance = it?.location?.distance)
            )
        }
        adapter.setList(list)
        if (isInFirstState) isInFirstState = false
    }

    fun onMapReady(gm: GoogleMap) {
        mMap = gm
    }


    fun setCurrentMarker(latLng: LatLng) {
        if (currentMarker == null) {
            currentMarker =
                mMap?.getMarker(latLng, BitmapDescriptorFactory.HUE_BLUE, true) //current marker
            mMap?.setOnCameraIdleListener(this)
            mMap?.setOnCameraMoveListener(this)
        } else {
            mMap?.moveMarkerToPosition(currentMarker, latLng)
        }
    }

    private fun setSelectedMarker(latLng: LatLng) {
        if (vMarker == null)
            vMarker = mMap?.getMarker(latLng,
                BitmapDescriptorFactory.HUE_RED,
                true) // venue selected marker
        else {
            mMap?.moveMarkerToPosition(vMarker, latLng, true)
        }
    }


    fun returnToFirstState() {
        currentMarker?.position?.let {
            mMap?.animateCameraToPosition(it, false)
            isInFirstState = true
            currentItem.value = null
            notifyChange()
        }
    }

    override fun onCameraIdle() {
        if (isInFirstState && !turnOnCameraIdleFun) return
        val lat: Double? = mMap?.cameraPosition?.target?.latitude
        val lng: Double? = mMap?.cameraPosition?.target?.longitude
        if (lat == null || lng == null) {
            return
        }
        Timber.e("idle on")
        val selectedLatLng = LatLng(lat, lng)
        getVenues(selectedLatLng)
    }

    override fun onCameraMove() {
        turnOnCameraIdleFun = true
    }
}

