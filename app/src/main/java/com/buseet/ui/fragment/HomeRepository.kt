package com.buseet.ui.fragment

import com.buseet.base.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val apiService: ApiService
) {


    fun getVenues(request: VenuesRequest) = flow {
        val res = apiService.getVenues(request.clientId,
            clientSecret = request.clientSecret,
            version = request.version,
            latLong = "${request.latLong.latitude},${request.latLong.longitude}",
            categoryId = request.categoryId)
        emit(res)
    }.flowOn(Dispatchers.IO).catch { Timber.e(it) }

}