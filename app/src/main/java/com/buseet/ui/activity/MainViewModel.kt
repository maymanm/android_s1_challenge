package com.buseet.ui.activity

import android.app.Application
import androidx.databinding.ObservableBoolean
import com.buseet.base.AndroidBaseViewModel

class MainViewModel(app: Application) : AndroidBaseViewModel(app) {
    var obsShowProgress: ObservableBoolean = ObservableBoolean()
}