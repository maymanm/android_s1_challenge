package com.buseet.app

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Base64
import android.view.View
import android.widget.*
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.buseet.R
import com.buseet.util.*
import timber.log.Timber


/**
 * Created by MahmoudAyman on 6/18/2020.
 **/
interface OnTabChangeCallBack {
    fun onChange(pos: Int)
}

class OtherViewsBinding {
    companion object{
        private const val IMAGE_TAG = "load_image"
    }
    @BindingAdapter(value = ["loadImage", "imageLoader"], requireAll = false)
    fun bindLoadImage(imageView: ImageView, obj: Any?, progressBar: ProgressBar?) {
        obj?.let {
            when (it) {
                is Int -> imageView.setImageResource(it)
                is Drawable -> imageView.setImageDrawable(it)
                is Bitmap -> imageView.setImageBitmap(it)
                is Uri -> imageView.setImageURI(it)
                is String -> when {
                    it.isValidUrl() -> imageView.loadImageFromURL(it, progressBar)
                    it.length >= 200 -> {
                        Timber.tag(IMAGE_TAG).i("image is encoded")
                        val decodedString: ByteArray = Base64.decode(obj.toString(), Base64.DEFAULT)
                        Glide.with(imageView.context).asBitmap()
                            .load(decodedString)
                            .error(R.drawable.ic_broken_image)
                            .into(imageView)
                    }
                    else -> {
                        Timber.tag(IMAGE_TAG).e("image string isn't valid")
                        loadEmptyImage(imageView, progressBar)
                    }
                }
                else -> {
                    Timber.tag(IMAGE_TAG).e("image url isn't valid")
                    loadEmptyImage(imageView, progressBar)
                }
            }
        } ?: loadEmptyImage(imageView, progressBar)
    }

    private fun loadEmptyImage(imageView: ImageView, progressBar: ProgressBar?) {
        imageView.loadImageFromURL(null, progressBar)
    }

    @BindingAdapter("app:visibleGone")
    fun bindViewGone(view: View, b: Boolean) {
        if (b) {
            view.visible()
        } else
            view.gone()
    }


    @BindingAdapter("adapter", "setHasFixedSize", requireAll = false)
    fun bindAdapter(recyclerView: RecyclerView, adapter: ListAdapter<*, *>?, hasFixedSize:Boolean?) {
        adapter?.let {
            recyclerView.adapter = it
            hasFixedSize?.let {
                recyclerView.setHasFixedSize(hasFixedSize)
            }
        } ?: Timber.e("adapter is null")
    }

}