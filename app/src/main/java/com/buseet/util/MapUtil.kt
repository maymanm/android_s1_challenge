package com.buseet.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.res.Resources
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import timber.log.Timber
import java.util.*


object MapUtil {
    fun Context.isGPSEnabled(): Boolean {
        val manager = (getSystemService(Context.LOCATION_SERVICE) as LocationManager)
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun openGPSFromSettings(
        register: ActivityResultLauncher<Intent>?,
    ) {
        register?.launch(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }

    private fun getMarkerOptions(
        latLng: LatLng,
        cursorColor: Float,
    ): MarkerOptions {
        return MarkerOptions()
            .position(latLng)
            .icon(BitmapDescriptorFactory.defaultMarker(cursorColor))
    }

    fun GoogleMap.moveMarkerToPosition(marker: Marker?, latLng: LatLng, zoomIn: Boolean = false) {
        marker?.position = latLng
        if (zoomIn)
            animateCameraToPosition(latLng, false)
    }

    fun GoogleMap.getMarker(
        latLng: LatLng,
        color: Float,
        zoomIn: Boolean = false,
    ): Marker? {
        if (zoomIn)
            animateCameraToPosition(latLng, false)
        val markerOptions = getMarkerOptions(latLng, color)
        return addMarker(markerOptions)
    }

    fun GoogleMap.getMarker(
        lat: String?,
        lng: String?,
        zoomIn: Boolean = false,
    ): Marker? {
        val latLng = LatLng(lat?.toDouble() ?: 0.0, lng?.toDouble() ?: 0.0)
        if (zoomIn)
            animateCameraToPosition(latLng, false)
        val markerOptions = getMarkerOptions(latLng, BitmapDescriptorFactory.HUE_VIOLET)
        return addMarker(markerOptions)
    }

    fun GoogleMap.animateCameraToPosition(
        latLng: LatLng,
        animate: Boolean,
    ): GoogleMap {
        val factory = CameraUpdateFactory.newLatLngZoom(latLng, 17f)
        if (animate) animateCamera(factory) else {
            moveCamera(factory)
        }
        return this
    }

    @JvmStatic
    @SuppressLint("MissingPermission")
    fun requestLocationUpdates(
        activity: FragmentActivity,
        fusedLocationClient: FusedLocationProviderClient?,
        callback: (Location) -> Unit,
    ): LocationCallback {
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 10000
            fastestInterval = 5000
        }
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    Timber.e("couldn't get location update")
                } else {
                    Timber.tag("location updates").i("$locationResult")
                    if (locationResult.locations.size > 0) {
                        val location = locationResult.locations[0]
                        callback(location)
                        Timber.i("$location")
                    }
                }
            }

        }
        try {
            if (PermissionUtil.hasAllLocationPermissions(activity))
                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.getMainLooper()
                )
        } catch (e: Exception) {
            Timber.e(e)
        }
        return locationCallback
    }
}