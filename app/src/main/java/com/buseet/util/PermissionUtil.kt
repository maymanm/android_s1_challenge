package com.buseet.util

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import timber.log.Timber

object PermissionUtil {
    private fun isGranted(activity: Context, permission: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    enum class AppPermissionResult {
        ALL_GOOD, OPEN_SETTING, FAIL
    }

    private fun Context.runDexter(
        permissions: List<String>,
        callBack: (AppPermissionResult) -> Unit,
    ) {
        Dexter.withContext(this)
            .withPermissions(permissions).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    when {
                        report.areAllPermissionsGranted() -> {
                            callBack(AppPermissionResult.ALL_GOOD)
                        }
                        report.isAnyPermissionPermanentlyDenied -> {
                            callBack(AppPermissionResult.OPEN_SETTING)
                        }
                        else -> {
                            callBack(AppPermissionResult.FAIL)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    list: List<PermissionRequest>,
                    token: PermissionToken,
                ) {
                    Timber.e("hg")
                    token.continuePermissionRequest()
                }
            }).check()
    }

    fun goToSettingsPermissions(context: Context,
        register: ActivityResultLauncher<Intent>?,
    ) {
        val myAppSettings = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:${context.packageName}")
        )
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        register?.launch(myAppSettings)
    }


    fun Context.requestAppPermissions(
        permissions: List<String>,
        callBack: (AppPermissionResult) -> Unit,
    ) {
        runDexter(permissions, callBack)
    }

    fun hasAllLocationPermissions(context: Context, includeBGLocation: Boolean = false): Boolean {
        return if (includeBGLocation) {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                isGranted(context, Manifest.permission.ACCESS_FINE_LOCATION) &&
                        isGranted(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            } else {
                isGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
            }
        } else
            isGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
    }

    fun getAllLocationPermissions(includeBGLocation: Boolean = false): List<String> {
        val permissionList = mutableListOf(Manifest.permission.ACCESS_FINE_LOCATION)
        if (includeBGLocation)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
        return permissionList
    }

}