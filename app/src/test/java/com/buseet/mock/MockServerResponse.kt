package com.buseet.mock

object MockServerResponse {
    const val validResponse =
        "{\n" +
                "   \"meta\":{\n" +
                "      \"code\":200,\n" +
                "      \"requestId\":\"6107b91a3082371787654e6e\"\n" +
                "   },\n" +
                "   \"response\":{\n" +
                "      \"venues\":[\n" +
                "         {\n" +
                "            \"id\":\"4c5ef77bfff99c74eda954d3\",\n" +
                "            \"name\":\"Eataly Flatiron\",\n" +
                "            \"location\":{\n" +
                "               \"address\":\"200 5th Ave\",\n" +
                "               \"crossStreet\":\"at W 23rd St\",\n" +
                "               \"lat\":40.74198696405861,\n" +
                "               \"lng\":-73.98991319280194,\n" +
                "               \"labeledLatLngs\":[\n" +
                "                  {\n" +
                "                     \"label\":\"display\",\n" +
                "                     \"lat\":40.74198696405861,\n" +
                "                     \"lng\":-73.98991319280194\n" +
                "                  }\n" +
                "               ],\n" +
                "               \"distance\":334,\n" +
                "               \"postalCode\":\"10010\",\n" +
                "               \"cc\":\"US\",\n" +
                "               \"city\":\"New York\",\n" +
                "               \"state\":\"NY\",\n" +
                "               \"country\":\"United States\",\n" +
                "               \"formattedAddress\":[\n" +
                "                  \"200 5th Ave (at W 23rd St)\",\n" +
                "                  \"New York, NY 10010\",\n" +
                "                  \"United States\"\n" +
                "               ]\n" +
                "            },\n" +
                "            \"categories\":[\n" +
                "               {\n" +
                "                  \"id\":\"4bf58dd8d48988d1f5941735\",\n" +
                "                  \"name\":\"Gourmet Shop\",\n" +
                "                  \"pluralName\":\"Gourmet Shops\",\n" +
                "                  \"shortName\":\"Gourmet\",\n" +
                "                  \"icon\":{\n" +
                "                     \"prefix\":\"https://ss3.4sqi.net/img/categories_v2/shops/food_gourmet_\",\n" +
                "                     \"suffix\":\".png\"\n" +
                "                  },\n" +
                "                  \"primary\":true\n" +
                "               }\n" +
                "            ],\n" +
                "            \"delivery\":{\n" +
                "               \"id\":\"2150992\",\n" +
                "               \"url\":\"https://www.seamless.com/menu/eataly-nyc-flatiron-200-5th-ave-new-york/2150992?affiliate=1131&utm_source=foursquare-affiliate-network&utm_medium=affiliate&utm_campaign=1131&utm_content=2150992\",\n" +
                "               \"provider\":{\n" +
                "                  \"name\":\"seamless\",\n" +
                "                  \"icon\":{\n" +
                "                     \"prefix\":\"https://fastly.4sqi.net/img/general/cap/\",\n" +
                "                     \"sizes\":[\n" +
                "                        40,\n" +
                "                        50\n" +
                "                     ],\n" +
                "                     \"name\":\"/delivery_provider_seamless_20180129.png\"\n" +
                "                  }\n" +
                "               }\n" +
                "            },\n" +
                "            \"referralId\":\"v-1627896090\",\n" +
                "            \"hasPerk\":false\n" +
                "         },\n" +
                "         {\n" +
                "            \"id\":\"5c5b91da475abd002c39f4f8\",\n" +
                "            \"name\":\"Happy Cow\",\n" +
                "            \"location\":{\n" +
                "               \"lat\":40.683025,\n" +
                "               \"lng\":-73.97572,\n" +
                "               \"labeledLatLngs\":[\n" +
                "                  {\n" +
                "                     \"label\":\"display\",\n" +
                "                     \"lat\":40.683025,\n" +
                "                     \"lng\":-73.97572\n" +
                "                  }\n" +
                "               ],\n" +
                "               \"distance\":6767,\n" +
                "               \"postalCode\":\"11217\",\n" +
                "               \"cc\":\"US\",\n" +
                "               \"city\":\"Brooklyn\",\n" +
                "               \"state\":\"NY\",\n" +
                "               \"country\":\"United States\",\n" +
                "               \"formattedAddress\":[\n" +
                "                  \"Brooklyn, NY 11217\",\n" +
                "                  \"United States\"\n" +
                "               ]\n" +
                "            },\n" +
                "            \"categories\":[\n" +
                "               {\n" +
                "                  \"id\":\"4bf58dd8d48988d1d3941735\",\n" +
                "                  \"name\":\"Vegetarian / Vegan Restaurant\",\n" +
                "                  \"pluralName\":\"Vegetarian / Vegan Restaurants\",\n" +
                "                  \"shortName\":\"Vegetarian / Vegan\",\n" +
                "                  \"icon\":{\n" +
                "                     \"prefix\":\"https://ss3.4sqi.net/img/categories_v2/food/vegetarian_\",\n" +
                "                     \"suffix\":\".png\"\n" +
                "                  },\n" +
                "                  \"primary\":true\n" +
                "               }\n" +
                "            ],\n" +
                "            \"referralId\":\"v-1627896090\",\n" +
                "            \"hasPerk\":false\n" +
                "         }\n" +
                "      ],\n" +
                "      \"confident\":false\n" +
                "   }\n" +
                "}"
    const val notValidResponse = "{}"
}