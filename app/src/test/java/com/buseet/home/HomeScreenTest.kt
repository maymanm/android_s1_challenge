package com.buseet.home

import com.buseet.mock.MockServerResponse
import com.buseet.ui.fragment.HomeResponse
import com.google.gson.Gson
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class HomeScreenTest {

    @Test
    fun testResponseSuccess() {
        val json = Gson().fromJson(MockServerResponse.validResponse, HomeResponse::class.java)
        Assert.assertNotNull(json)
    }

    @Test
    fun testResponseFail() {
        val json = Gson().fromJson(MockServerResponse.notValidResponse, HomeResponse::class.java)
        assertNotEquals(
            json?.meta?.code,
            200
        )
    }

}